#  LIS4368 - Advanced Web Applications

## Bradley Patterson

### Project 1 Requirements:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of Homepage;
* Screenshot of Failed Validation;
* Screenshot of Successful Validation;
* Research on Validation codes;


#### Validation Codes:
	*valid: 'fa fa-check',
		* Shown when the field is valid
	*invalid: 'fa fa-times',
		* Shown when the field is invalid
	*validating: 'fa fa-refresh'
		* Shown when the field is being validated


#### Assignment Screenshots:

*Screenshot of Homepage*:

![Homepage Screenshot](img/hp.png)


*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/fv.png)


*Screenshot of Successful Validation*:

![Successful Validation Screenshot](img/sv.png)
