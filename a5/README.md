#  LIS4368 - Advanced Web Applications

## Bradley Patterson

### Assignment 5 Requirements:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of Form Filled Out
* Screenshot of Successful Validation
* Screenshot of Data


#### Assignment Screenshots:

*Screenshot of Filled Out Form*:

![Filled Form Screenshot](img/ff.png)


*Screenshot of Successful Validation*:

![Successful Validation Screenshot](img/sv.png)


*Screenshot of Data*:

![Data Screenshot](img/data.png)
