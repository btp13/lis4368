
# LIS4368 - Adv. Web Applications

## Bradley Patterson

### Assignment 1 Requirements:


#### README.md file should include the following items:

1. Screenshot of running java Hello (#1 above);
2. Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial);
3. git commands w/short descriptions;
4. Bitbucket repo links: a) this assignment and b) the completed tutorial repos above
(bitbucketstationlocations and myteamquotes).


#### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Shows the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git stash - Stash the changes in a dirty working directory away


#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running http://localhost:9999*:

![Tomcat Screenshot](img/tomcat.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/btp13/bitbucketstationlocations "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/btp13/myteamquotes"My Team Quotes Tutorial")
