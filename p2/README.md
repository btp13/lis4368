#  LIS4368 - Advanced Web Applications

## Bradley Patterson

### Project 2 Requirements:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of Valid Entry;
* Screenshot of Passed Validation;
* Screenshot of Display Data;
* Screenshot of Modify;
* Screenshot of updated data;
* Screenshot of Delete Warning;
* Screenshot of Database Data;

#### Assignment Screenshots:

*Screenshot of Valid Entry*:

![Entry Screenshot](img/valid.png)


*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passed.png)


*Screenshot of Display Data*:

![Display Data Screenshot](img/data.png)

*Screenshot of Modify*:

![modify Screenshot](img/modify.png)


*Screenshot of updated data*:

![updated data Screenshot](img/updated.png)


*Screenshot of Delete Warning*:

![warning Screenshot](img/warning.png)


*Screenshot of Database Data*:

![data Screenshot](img/database.png)