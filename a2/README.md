
# LIS4368 - Adv. Web Applications

## Bradley Patterson

### Assignment 2 Requirements:


#### README.md file should include the following items:

1. [Hello](http://localhost:9999/hello "displays directory")
2. [Index](http://localhost:9999/hello/HelloHome.html "displays index.html")
3. [sayhello](http://localhost:9999/hello/sayhello "invokes HelloServlet")
4. [querybook](http://localhost:9999/hello/querybook.html "querybook")
5. [sayhi](http://localhost:9999/hello/sayhi "invokes AnotherHelloServlet")


#### Assignment Screenshots:

*Screenshot of Hello*:

![Directory Screenshot](img/directory.png)

*Screenshot of Index:

![Index.html Screenshot](img/index.png)

*Screenshot of Sayhello:

![Sayhello Screenshot](img/sayhello.png)

*Screenshot of Sayhi:

![Sayhi Screenshot](img/sayhi.png)

*Screenshot of Query Book:

![queryboook Screenshot](img/querybook.png)

*Screenshot of Selected Item:

![selecteditem Screenshot](img/selecteditem.png)

*Screenshot of Query Results:

![queryresults Screenshot](img/results.png)
