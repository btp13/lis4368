#  LIS4368 - Advanced Web Applications

## Bradley Patterson

### Assignment 3 Requirements:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of ERD;
* Links to the following files:
	* a3.mwb
	* a3.sql


#### Assignment Screenshots:

*Screenshot of ERD*:

![Healthy Recipe app Screen 1 Screenshot](img/a3.png)

	
#### Assignment Links: 

*Link to a3.mwb*
[a3.mwb](a3.mwb)

*Link to a3.sql*
[a3.sql](a3.sql)
