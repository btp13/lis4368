-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema btp13
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `btp13` ;

-- -----------------------------------------------------
-- Schema btp13
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `btp13` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `btp13` ;

-- -----------------------------------------------------
-- Table `btp13`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `btp13`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `btp13`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT NOT NULL,
  `pst_phone` BIGINT NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `btp13`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `btp13`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `btp13`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT NOT NULL,
  `cus_phone` BIGINT NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `btp13`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `btp13`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `btp13`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NOT NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_nueter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `btp13`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `btp13`.`customer` (`cus_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `btp13`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `btp13`;
INSERT INTO `btp13`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'paws and claws', '123 xbox lane', 'Tallahassee', 'FL', 32304, 8501234567, 'paws.claws2gmail.com', 'pawsandclaws.com', 7654.00, 'sharpen those claws');
INSERT INTO `btp13`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'petsmart', '456 tv rd', 'Jacksonville', 'FL', 32217, 9041234567, 'petsmart@aol.com', 'petsmart.com', 9875.46, NULL);
INSERT INTO `btp13`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'pets-r-us', '789 mouse way', 'Charlotte', 'NC', 12345, 1234567890, 'pets.r.us@yahoo.com', NULL, 4765.87, NULL);
INSERT INTO `btp13`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'pet-tastic', '142 moniter blvd', 'Pheonix', 'AZ', 54321, 2345678901, 'pet.tastic@gmail.com', NULL, 245.00, 'have a pet-tastic day');
INSERT INTO `btp13`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'pet shack', '947 cup rd', 'Dallas', 'TX', 23456, 3456789012, 'pet.shack@gmail.com', 'petshack.com', 124543.00, NULL);
INSERT INTO `btp13`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'pet world', '193 remote way', 'Tampa', 'FL', 65432, 4567890123, 'pet.world@gmail.com', NULL, 4363.89, NULL);
INSERT INTO `btp13`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'pet dynasty', '859 headphones blvd', 'Miami', 'FL', 98765, 5678901234, 'pet.dynasty@gmail.com', NULL, 235.00, NULL);
INSERT INTO `btp13`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'pet empire', '938 flashlight lane', 'Athens', 'GA', 56789, 6789012345, 'pet.empire@gmail.com', 'petempire.com', 98754.65, 'rise of the pet empire');
INSERT INTO `btp13`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'pet emporium', '557 knife pt', 'Atlanta', 'GA', 67890, 7890123456, 'pet.emporium@gmail.com', NULL, 3245.44, NULL);
INSERT INTO `btp13`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'pet labs', '495 keys rd', 'Daytona', 'FL', 76523, 8901234567, 'pet.labs@yahoo.com', NULL, 63467.00, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `btp13`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `btp13`;
INSERT INTO `btp13`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'bob', 'larson', '123 case way', 'Tallahassee', 'FL', 32304, 8501234567, 'bob@gmail.com', 100.00, 100.00, 'dog');
INSERT INTO `btp13`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'bill', 'board', '456 sticker rd', 'Jacsonville', 'FL', 32217, 9041234567, 'bill@aol.com', 50.00, 50.00, NULL);
INSERT INTO `btp13`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'brad', 'patt', '353 lemon loop', 'Charlotte', 'NC', 12345, 1234567890, 'brad@yahoo.com', 657.00, 657.00, NULL);
INSERT INTO `btp13`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'james', 'nickel', '575 bark trail', 'Pheonix', 'AZ', 67890, 2345678901, 'james@gmail.com', 643.00, 643.00, 'cat');
INSERT INTO `btp13`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'nick', 'rancher', '985 silly rd', 'Dallas', 'TX', 54321, 3456789012, 'nick@gmail.com', 45.89, 45.89, NULL);
INSERT INTO `btp13`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'chuck', 'finley', '495 pig way', 'Tampa', 'FL', 90876, 4567890123, 'chuck@gmail.com', 45.80, 45.80, NULL);
INSERT INTO `btp13`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'john', 'travelston', '982 desk lane', 'Miami', 'FL', 23456, 5678901234, 'john@gmail.com', 235.00, 235.00, 'magic');
INSERT INTO `btp13`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'phil', 'barnes', '987 nightstand blvd', 'Athens', 'GA', 64367, 6789012345, 'phil@gmail.com', 1000.00, 1000.00, 'pure bred');
INSERT INTO `btp13`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'mike', 'tyson', '865 bed pt', 'Atlanta', 'GA', 34678, 7890123456, 'mike@gmail.com', 456.00, 456.00, NULL);
INSERT INTO `btp13`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'tom', 'jones', '567 pillow blvd', 'Daytona', 'FL', 54637, 8901234567, 'tom@gmail.com', 23.00, 23.00, 'pet food');

COMMIT;


-- -----------------------------------------------------
-- Data for table `btp13`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `btp13`;
INSERT INTO `btp13`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nueter`, `pet_notes`) VALUES (DEFAULT, 1, 1, 'dog', 'm', 50.00, 100.00, 1, 'black', '2001-01-01', 'y', DEFAULT, 'is adorable');
INSERT INTO `btp13`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nueter`, `pet_notes`) VALUES (DEFAULT, 2, 2, 'cat', 'f', 50.00, 100.00, 2, 'white', '2002-02-02', 'y', 'y', 'plotting to kill us all');
INSERT INTO `btp13`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nueter`, `pet_notes`) VALUES (DEFAULT, 3, NULL, 'hamster', 'm', 25.00, 75.00, 3, 'orange', '2003-03-03', 'y', 'y', NULL);
INSERT INTO `btp13`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nueter`, `pet_notes`) VALUES (DEFAULT, 4, NULL, 'fish', 'm', 25.00, 75.00, 4, 'brown', '2004-04-04', 'n', 'y', NULL);
INSERT INTO `btp13`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nueter`, `pet_notes`) VALUES (DEFAULT, 5, 5, 'snake', 'f', 25.00, 75.00, 5, 'green', '2005-05-05', 'n', 'y', NULL);
INSERT INTO `btp13`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nueter`, `pet_notes`) VALUES (DEFAULT, 6, NULL, 'mouse', 'f', 25.00, 75.00, 6, 'white', '2006-06-06', 'y', 'n', NULL);
INSERT INTO `btp13`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nueter`, `pet_notes`) VALUES (DEFAULT, 7, 7, 'bird', 'f', 35.00, 75.00, 7, 'blue', '2007-07-07', 'n', 'n', NULL);
INSERT INTO `btp13`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nueter`, `pet_notes`) VALUES (DEFAULT, 8, NULL, 'tiger', 'f', 100.00, 250.00, 8, 'orange', '2008-08-08', 'y', 'n', 'attacked sam');
INSERT INTO `btp13`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nueter`, `pet_notes`) VALUES (DEFAULT, 9, 9, 'monkey', 'm', 75.00, 150.00, 9, 'brown', '2009-09-09', 'y', 'n', 'likes bananas');
INSERT INTO `btp13`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_nueter`, `pet_notes`) VALUES (DEFAULT, 10, 10, 'bear', 'm', 150.00, 300.00, 10, 'black', '2010-10-10', 'y', 'n', 'killed greg');

COMMIT;

