#  LIS4368 - Advanced Web Applications

## Bradley Patterson

### Assignment 4 Requirements:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of Failed Validation
* Screenshot of Successful Validation


#### Assignment Screenshots:

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/fv.png)


*Screenshot of Successful Validation*:

![Successful Validation Screenshot](img/sv.png)
