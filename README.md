
# LIS4368

## Bradley Patterson

### Assignment Requirements:


*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
	- Git commands
	- link to Bitbucket Tutorial - Station Locations
	- link to Tutorial: Request to update a teammate's repository

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- [Hello](http://localhost:9999/hello "displays directory")
	- [Index](http://localhost:9999/hello/HelloHome.html "displays index.html")
	- [sayhello](http://localhost:9999/hello/sayhello "invokes HelloServlet")
	- [querybook](http://localhost:9999/hello/querybook.html "querybook")
	- [sayhi](http://localhost:9999/hello/sayhi "invokes AnotherHelloServlet")

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Course title, your name, assignment requirements, as per A1;
	- Screenshot of ERD;
	- Links to the following files:
		- a3.mwb
		- a3.sql
	
4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Course title, your name, assignment requirements, as per A1;
	- Screenshot of Failed Validation
	- Screenshot of Successful Validation
	
5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Course title, your name, assignment requirements, as per A1;
	- Screenshot of Form Filled Out
	- Screenshot of Successful Validation
	- Screenshot of Data
	
6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Course title, your name, assignment requirements, as per A1;
	- Screenshot of Homepage;
	- Screenshot of Failed Validation;
	- Screenshot of Successful Validation;
	- Research on Validation codes;
	
7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Course title, your name, assignment requirements, as per A1;
	- Screenshot of Valid Entry;
	- Screenshot of Passed Validation;
	- Screenshot of Display Data;
	- Screenshot of Modify;
	- Screenshot of updated data;
	- Screenshot of Delete Warning;
	- Screenshot of Database Data;
